# Disk Usage Reporter

This lightweight Ruby program observes the disk usage and notifies the
user when a given quota is reached.

## Features

 - specify as many drives to observe as you want
 - set a global quota or override per drive when the user should be
   notified (default is 5 GB)
 - set an interval in seconds when the program should calculate free
   disk space (default is 10 minutes)
 - customize the hostname which should show up in reports to keep
   track of your servers easily (default is `[auto]` which uses the
   current hostname of the machine)
 - send e-mail reports or do nothing to test the configuration (log to console)
 - run in foreground for debugging or run as daemon in background for production
 - optimal file logging (not recommended without log rotation)

## Installation

##### Requirements

 - Ruby 2.5
 - RubyGems and Bundler

##### Setup

 - run `bundle install --path vendor` to install dependencies <br>
   note: you can also install dependencies globally if you want
 - then just do `./disk-usage-reporter` to start the program in debug mode
 - for production use `./disk-usage-reporter-d`

## Testing

You can test e-mail sending with `test/mail.rb`. Make sure to configure
the correct e-mail addresses first. Errors will be printed in the console.

## Configuration

The configuration file can be found in `~/.config/disk-usage-report/`.
The program will generate one with default values on first start, so run
it once. Don't worry, the default notify type is set to `none`.

An example config file looks like this:
```json
{
  "log": false,
  "hostname": "[auto]",
  "devices": [
    {
      "dev": "/",
      "quota": "default"
    },
    {
      "dev": "/home",
      "quota": 4194304
    }
  ],
  "interval": 60,
  "quota": 5242880,
  "notify": "none",
  "notify_mail_to_address": "root@localhost",
  "notify_mail_from_address": "disk-usage-reporter@localhost",
  "notify_mail_subject": "Alert: Disk space running low on \"$HOSTNAME\" (on $DEVICE, free $DEV_FREE_SPACE)"
}
```

 - `log`: \[boolean\] <br>
   Whenever a log file should be written to disk or not.

 - `hostname`: \[string\] <br>
   Override the hostname which should show in reports. When the value is
   set to `[auto]`, the default system hostname will be used. Spaces are
   supported.

 - `devices`: \[array\] <br>
   An array of JSON objects `{"dev": "", "quota": ""}`. The value of `dev`
   is the mounted path of the drive. The value of `quota` is the amount of
   free space remaining to send out a warning. The quota can also have a
   value of `default` which will use the global quota.

 - `interval`: \[integer\] <br>
   The interval in seconds when the script should check free disk space.
   Don't set this too low as it may degredate the performance of your
   server. Default is 10 minutes.

 - `quota`: \[integer\] <br>
   The quota in kilobytes (1024 notation) of free remaining disk space to
   send out a warning.

 - `notify`: \[string\] <br>
   The notification type. Currently there are only 2 supported types.
    - `none`: Do nothing when the quota is reached. Useful for debugging
      the configuration before starting the program in production mode.
    - `mail`: Send out e-mail reports when the quota is reached. E-Mails
      are always sent from localhost via SMTP. External mail servers are
      currently not supported. The `from` and `to` addresses can be changed
      with the options `notify_mail_from_address` and `notify_mail_to_address`.

### E-Mail Subject Customization

The `notify_mail_subject` option has some placeholders to create your
own unique subject line. The default is `Alert: Disk space running low on "$HOSTNAME" (on $DEVICE, free $DEV_FREE_SPACE)`.

Available placeholders are:
 - `$HOSTNAME`: replaced with hostname
 - `$DEVICE`: name of the device the alert is for
 - `$DEV_FREE_SPACE`: free space left on the device in MB

## License

Copyright (C) 2018 Lena Stöffler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
