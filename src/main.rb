#
# Main Application Script
#

require "sys/filesystem"
require "net/smtp"
require "date"

require_relative "config"

module App

@@config = nil

def self.config
  return @@config
end

def self.init()
  # Load app configuration
  @@config = Config.new

  case @@config.status
    when Config::State::UNKNOWN || Config::State::PARSE_ERROR
      p "Unable to read config file!", @@config.file
  end
end

def self.main_loop()
  # always get the device list for each iteration, the function checks if the drive is still mounted and
  # temporarily removes those entries from the list, otherwise the disk space check would throw an exception
  @@config.devices.each do |dev|
    if dev[:quota] >= free_disk_space(dev[:dev])
      p "Disk usage warning on #{dev[:dev]}! Quota reached: #{dev[:quota]} kilobytes"
      case @@config[Config::Keys::notify]
        # send e-mail
        when Config::NotifyType::Mail
          send_mail(dev[:dev], @@config[Config::Keys::notify_mail_to_address], @@config[Config::Keys::notify_mail_from_address])
      end
    end
  end
end

def self.free_disk_space(device)
  stat = Sys::Filesystem.stat(device)
  kilobytes_available = stat.block_size * stat.blocks_available / 1024
  p format("Free disk space on %<dev>s at %<time>s: %<free_space>s kilobytes", dev: device, time: DateTime.now, free_space: kilobytes_available)
  return kilobytes_available
end

def self.send_mail(dev, to_addr, from_addr)
  p format("Sending e-mail to %<to_addr>s", to_addr: to_addr)

  mb_free = free_disk_space(dev) / 1024

  message = <<~MSG
    From: Disk Usage Reporter <#{from_addr}>
    To: #{to_addr} <#{to_addr}>
    Subject: #{@@config.mail_subject dev, mb_free}

    Disk Usage Reporter has detected that you are running low on disk space on "#{@@config.hostname}".
    Currently there is #{mb_free}MB of free space left on the device "#{dev}".

    Consider freeing up some space quickly or upgrade the storage!
  MSG

  #p message

  begin
    Net::SMTP.start("localhost") do |smtp|
      smtp.send_message message, from_addr, to_addr
      p "E-Mail sent!"
    end
  rescue
    p "Failed to send e-mail! SMTP error occurred."
  end
end

end
