#
# Configuration Loader
#

require "fileutils"
require "socket"
require "ruby-enum"
require "json"

class Config

  # status codes
  class State
    include Ruby::Enum

    define :UNKNOWN, -1
    define :OK, 0
    define :PARSE_ERROR, 1
  end

  # notify types
  class NotifyType
    include Ruby::Enum

    define :None, "none".freeze
    define :Mail, "mail".freeze
  end

  # key names
  class Keys
    include Ruby::Enum

    define :log, "log".freeze             # bool: should write output to log file?
    define :hostname, "hostname".freeze  # hostname to mention in notifications, defaults to "[auto]" == #{Socket.gethostname}
    define :devices, "devices".freeze    # devices to watch
    define :interval, "interval".freeze  # time in seconds between checks
    define :quota, "quota".freeze        # default quota warning in kilobytes
    define :notify, "notify".freeze      # notify type, possible values are ["none", "mail"]
    define :notify_mail_to_address, "notify_mail_to_address".freeze
    define :notify_mail_from_address, "notify_mail_from_address".freeze
    define :notify_mail_subject, "notify_mail_subject".freeze
  end

  # e-mail subject placeholders
  class SubjectPlaceholders
    include Ruby::Enum

    define :hostname, "$HOSTNAME"
    define :device, "$DEVICE"
    define :device_free_space, "$DEV_FREE_SPACE"
  end

  def initialize()
    # instance variables
    @status = State::UNKNOWN

    # Figure out config directory using $HOME variable
    @config_dir = File.join(Dir.home, ".config/disk-usage-reporter")
    @log_dir = File.join(@config_dir, "log")
    @config_file = File.join(@config_dir, "config.json")

    # Load default config
    load_default()

    # Create config directory and file if they don't exist
    FileUtils::mkdir_p @config_dir
    FileUtils::mkdir_p @log_dir
    unless File.exist? @config_file or File.empty? @config_file
      write_default()
    end

    # Parse config file
    begin
      @json = JSON.parse File.read(@config_file, mode: "r")
      @status = State::OK
    rescue JSON::ParserError
      @status = State::PARSE_ERROR
    end

    # Load default config when not OK
    if @status != State::OK
      load_default()
    end
  end

  # config file status (default is unknown)
  def status
    return @status
  end

  # Returns the config directory root path
  def dir
    return @config_dir
  end

  # Returns the log directory
  def log_dir
    return @log_dir
  end

  # Returns the main config file path
  def file
    return @config_file
  end

  # Returns the hostname
  def hostname
    return @json[Keys::hostname] == "[auto]" ? Socket.gethostname : @json[Keys::hostname]
  end

  # Returns the subject line
  def mail_subject(current_device, current_device_free_space)
    subject = @json[Keys::notify_mail_subject]
    subject = subject.gsub(SubjectPlaceholders::hostname, self.hostname)
    subject = subject.gsub(SubjectPlaceholders::device, current_device)
    subject = subject.gsub(SubjectPlaceholders::device_free_space, current_device_free_space.to_s + "MB")
    return subject
  end

  # Return watched devices
  def devices
    dev = []
    @json[Keys::devices].each do |d|
      if d.key?("dev") && d.key?("quota")
        if File.exist? d["dev"]
          d["quota"] == "default" ? \
            dev << {dev: d["dev"], quota: @json[Keys::quota]} : \
            dev << {dev: d["dev"], quota: d["quota"]}
        else
          p "Warning: drive \"#{d["dev"]}\" not mounted (anymore)."
        end
      else
        p "Warning: invalid device config found in \"devices\" array!"
      end
    end
    return dev
  end

  # operator[]
  def [](key)
    return @status == State::OK ? @json[key] : nil
  end


private

  def load_default()
    @DEFAULTS = {
      log: false,
      hostname: "[auto]",
      devices: [{dev: "/", quota: "default"}],
      interval: 600,
      quota: 5_242_880,
      notify: NotifyType::None,
      notify_mail_to_address: "root@localhost",
      notify_mail_from_address: "disk-usage-reporter@localhost",
      notify_mail_subject: "Alert: Disk space running low on \"$HOSTNAME\" (on $DEVICE, free $DEV_FREE_SPACE)",
    }

    @json = @DEFAULTS
  end

  def write_default()
    f = File.open @config_file, "w"
    f.write JSON.pretty_generate(@DEFAULTS)
    f.close
  end

end
