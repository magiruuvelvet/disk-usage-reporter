#!/usr/bin/env ruby
#
# Test e-mail sending
#
#   uses the root drive by default
#

require_relative "../src/main"

config = Config.new

App::init()
App::send_mail "/", config[Config::Keys::notify_mail_to_address], config[Config::Keys::notify_mail_from_address]
